﻿using HotelManagement_IAction;
using HotelManagement_IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement_Action
{
    public class UserAction : IUserAction
    {
        private readonly IUserService _userService;

        public UserAction(IUserService userService)
        {
            _userService = userService;
        }
    }
}
