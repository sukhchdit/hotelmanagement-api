﻿using HotelManagement_IService;
using HotelManagement_Model.Dto;
using HotelManagement_Model.Entities;
using HotelManagement_Model.Helpers;
using HotelManagement_Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement_Service
{
    public class StaffService: IStaffService
    {
        private IRepository _rep;
        private readonly AppSetting _appSettings;

        public StaffService(IRepository rep, IOptions<AppSetting> appSettings)
        {
            _rep = rep;
            _appSettings = appSettings.Value;
        }

        public async Task CreateSuperAdmin()
        {
            var obj = new User
            {
                Email = Constants.SuperAdminEmail,
                Password = "SSP#1" + Constants.SuperAdminEmail,
                UserRole = UserRole.SuperAdmin,
                UserStatus = UserStatus.Active,
            };
            //await CreateStaff(obj);
        }

        public async Task DeleteStaff(Staff obj)
        {
            obj.Status = false;
            obj.User.Status = false;
            await _rep.SaveChangesAsync();
        }

        public async Task UpdateStaffStatus(Guid userId)
        {
            try
            {
                var staff = GetStaffByIdWithoutStatus(userId);
                if (staff == null)
                    throw new AppException("User not found");
                if (staff.User.UserStatus == UserStatus.Active)
                    staff.User.UserStatus = UserStatus.Suspended;
                else if (staff.User.UserStatus == UserStatus.Suspended)
                    staff.User.UserStatus = UserStatus.Active;
                await _rep.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new AppException("Could not update status.");
            }
        }

        public Staff GetStaffByIdWithoutStatus(Guid id)
        {
            return _rep.GetAll<Staff>(e => e.Id == id).Include(y => y.User).FirstOrDefault();
        }

        public Staff GetStaffById(Guid id)
        {
            return _rep.GetAll<Staff>(e => e.Id == id).Include(y => y.User).FirstOrDefault();
        }

        public Staff GetStaffByEmail(string email)
        {
            return _rep.GetAll<Staff>(e => e.User.Email == email).Include(y => y.User).FirstOrDefault();
        }

        public Staff GetStaffByEmailWithoutStatus(string email)
        {
            return _rep.GetAll<Staff>(e => e.User.Email == email).Include(y => y.User).FirstOrDefault();
        }

        public IEnumerable<Staff> GetAll(UserRole role)
        {
            return _rep.GetAll<Staff>(e => e.User.UserRole == role).Include(y => y.User);
        }

        public Staff StaffAuthenticate(LoginDto model)
        {
            if (string.IsNullOrEmpty(model.Email) || string.IsNullOrEmpty(model.Password))
                return null;
            //var user = _rep.Get<User>(x => x.Email == model.Email && x.UserRole == model.UserRole);
            var staff = _rep.GetAll<Staff>(x => x.User.Email == model.Email).FirstOrDefault();

            // check if username exists
            if (staff == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(model.Password, staff.User.PasswordHash, staff.User.PasswordSalt))
                return null;

            // authentication successful
            return staff;
        }

        public async Task<Staff> UpdateLastLoginTime(Staff model)
        {
            if (model != null)
            {
                model.User.LastLoginAt = DateTime.Now;
                _rep.Update(model);
                await _rep.SaveChangesAsync();
            }
            return model;
        }

        public string GetStaffToken(Staff staff)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, staff.Id.ToString()),
                    new Claim(ClaimTypes.Email, staff.User.Email),
                    new Claim(ClaimTypes.GivenName, staff.Name),
                    new Claim(ClaimTypes.Role, staff.User.UserRole.ToString()),
                    new Claim("firstName", staff.FirstName),
                    new Claim("lastName", staff.LastName),
                    new Claim("country",staff.Country!=null?staff.Country.ToString():""),
                    new Claim("phone",staff.Phone!=null?staff.Phone.ToString():""),
                    new Claim("lastLoginAt",staff.User.LastLoginAt!=null?staff.User.LastLoginAt.Value.ToShortDateString():"")
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        //public async Task CreateStaff(Staff model)
        //{
        //    _rep.Add(model);
        //    await _rep.SaveChangesAsync();
        //}

        public async Task UpdateStaff(Staff model)
        {
            _rep.Update(model);
            await _rep.SaveChangesAsync();
        }

        public Staff GetActiveStaffById(Guid id)
        {
            return _rep.Get<Staff>(x => x.Id == id && x.Status);
        }
        public async Task<Staff> CreateStaff(Staff staff)
        {
            // validation
            if (string.IsNullOrWhiteSpace(staff.User.Password))
            {
                staff.User.Password = "Hm#1" + staff.User.Email;
            }
            //    throw new AppException("Password is required");
            var oldUser = _rep.Get<User>(x => x.Email == staff.User.Email);
            if (oldUser != null)
                throw new AppException(string.Format("Email {0} is already taken", staff.User.Email));

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(staff.User.Password, out passwordHash, out passwordSalt);

            staff.User.PasswordHash = passwordHash;
            staff.User.PasswordSalt = passwordSalt;
            _rep.Add(staff.User);
            await _rep.SaveChangesAsync();

            return staff;
        }

        public async Task UpdatePassword(Staff obj, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);
            var model = _rep.Get<User>(x => x.Id == obj.User.Id);
            model.PasswordHash = passwordHash;
            model.PasswordSalt = passwordSalt;
            await _rep.SaveChangesAsync();
        }

        public async Task UpdatePassword(string newpassword, Staff staff)
        {
            if (string.IsNullOrWhiteSpace(newpassword))
                throw new AppException("Password is required");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(newpassword, out passwordHash, out passwordSalt);

            staff.User.PasswordHash = passwordHash;
            staff.User.PasswordSalt = passwordSalt;
            await _rep.SaveChangesAsync();
        }

        public async Task UpdateStaffDepartment(Department model, Staff staff)
        {
            var user = _rep.Get<User>(x => x.Id == staff.User.Id && staff.Status);
            if (staff != null)
            {
                var dept = _rep.Get<UserDepartment>(x => x.UserId == user.Id && x.DepartmentId == model.Id);
                if (dept != null)
                {
                    _rep.Add(new UserDepartment { DepartmentId = model.Id, UserId = staff.User.Id });
                }
                else
                {
                    dept.Status = true;
                }
                await _rep.SaveChangesAsync();
            }
        }

        public async Task Update(Staff userParam)
        {
            var staff = GetStaffById(userParam.Id);

            if (staff == null)
                throw new AppException("User not found");

            //update some user properties
            staff.User.UserRole = userParam.User.UserRole;
            staff.UpdatedBy = userParam.UpdatedBy;
            staff.User.UpdatedBy = userParam.User.UpdatedBy;
            staff.User.Email = userParam.User.Email;

            staff.FirstName = userParam.FirstName;
            staff.LastName = userParam.LastName;
            staff.Phone = userParam.Phone;
            staff.Country = userParam.Country;
            staff.User.UserRole = userParam.User.UserRole;
            staff.User.UserStatus = userParam.User.UserStatus;
            staff.User.UserDepartments = userParam.User.UserDepartments;
            await _rep.SaveChangesAsync();
        }

        public async Task ActivateUser(string email)
        {
            var staff = GetStaffByEmailWithoutStatus(email);

            if (staff == null)
                throw new AppException("User not found");

            staff.User.LastLoginAt = DateTime.Now;
            staff.User.UserStatus = UserStatus.Active;
            await _rep.SaveChangesAsync();
        }
        // private helper methods
        public void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        public async Task DeleteStaff(Guid id)
        {
            var staff = GetStaffByIdWithoutStatus(id);
            if (staff == null)
                throw new AppException("User not found");

            staff.Status = false;
            staff.User.Status = false;
            staff.User.UserStatus = UserStatus.Suspended;

            var userDepartments = _rep.GetAll<UserDepartment>(e => e.UserId == id).ToList();
            userDepartments.ForEach(ud =>
            {
                _rep.Delete(ud);
            });

            await _rep.SaveChangesAsync();

        }
    }
}
