﻿using HotelManagement_IService;
using HotelManagement_Model.Dto;
using HotelManagement_Model.Entities;
using HotelManagement_Model.Helpers;
using HotelManagement_Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement_Service
{
    public class UserService : IUserService
    {
        private IRepository _rep;

        public UserService(IRepository rep)
        {
            _rep = rep;
        }

        public User GetById(Guid id)
        {
            return _rep.Get<User>(x => x.Id == id);
        }

    }
}
