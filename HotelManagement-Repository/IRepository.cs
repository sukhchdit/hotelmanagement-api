﻿using HotelManagement_Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HotelManagement_Repository
{
    public interface IRepository
    {
        IQueryable<T> GetAll<T>(Expression<Func<T, bool>> predicate = null) where T : BaseEntity;

        T Get<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity;

        Task<T> Find<T>(long id) where T : BaseEntity;

        void Add<T>(T entity) where T : BaseEntity;

        void AddRange<T>(IEnumerable<T> list) where T : BaseEntity;

        void Update<T>(T entity) where T : BaseEntity;

        void Delete<T>(T entity) where T : BaseEntity;

        Task SaveChangesAsync();
        void SaveChanges();
    }
}
