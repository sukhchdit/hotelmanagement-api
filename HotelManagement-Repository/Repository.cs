﻿using HotelManagement_Context;
using HotelManagement_Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HotelManagement_Repository
{
    public class Repository : IRepository
    {
        private Context CurrentContext;

        public Repository(Context _currentContext)
        {
            CurrentContext = _currentContext;
        }

        public IQueryable<T> GetAll<T>(Expression<Func<T, bool>> predicate = null) where T : BaseEntity
        {
            if (predicate != null)
                return CurrentContext.Set<T>().Where(predicate);
            else
                return CurrentContext.Set<T>();
        }

        public T Get<T>(Expression<Func<T, bool>> predicate = null) where T : BaseEntity
        {
            try
            {
                if (predicate != null)
                    return CurrentContext.Set<T>().Where(predicate).FirstOrDefault();
                else
                    return CurrentContext.Set<T>().FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<T> Find<T>(long id) where T : BaseEntity
        {
            return await CurrentContext.Set<T>().FindAsync(id);
        }

        public void Add<T>(T entity) where T : BaseEntity
        {
            CurrentContext.Set<T>().AddAsync(entity);
        }

        public void Update<T>(T entity) where T : BaseEntity
        {
            CurrentContext.Set<T>().Update(entity);
        }

        public void Delete<T>(T entity) where T : BaseEntity
        {
            CurrentContext.Set<T>().Remove(entity);
        }

        public void AddRange<T>(IEnumerable<T> list) where T : BaseEntity
        {
            CurrentContext.Set<T>().AddRange(list);
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await CurrentContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveChanges()
        {
            try
            {
                CurrentContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
