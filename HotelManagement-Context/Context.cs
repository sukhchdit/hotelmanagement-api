﻿using HotelManagement_Model.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace HotelManagement_Context
{
    public class Context:DbContext
    {
        public Context(DbContextOptions<Context> options):base(options)
        {
        }

        #region savechanges

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity.GetType().BaseType.Name == "BaseEntity")
                {
                    var entity = (BaseEntity)entry.Entity;

                    if (entry.State == EntityState.Added)
                    {
                        entity.DateCreated = DateTime.Now;
                        entity.DateUpdated = DateTime.Now;
                        entity.Status = true;
                    }
                    else if (entry.State == EntityState.Modified)
                        entity.DateUpdated = DateTime.Now;
                }
            }
            return base.SaveChanges(true);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity.GetType().BaseType.Name == "BaseEntity")
                {
                    var entity = (BaseEntity)entry.Entity;

                    if (entry.State == EntityState.Added)
                    {
                        entity.DateCreated = DateTime.Now;
                        entity.DateUpdated = DateTime.Now;
                        entity.Status = true;
                    }
                    else if (entry.State == EntityState.Modified)
                        entity.DateUpdated = DateTime.Now;
                }
            }
            return await base.SaveChangesAsync(true, cancellationToken);
        }

        #endregion

        #region entities 

        public DbSet<User> Users { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<UserDepartment> UserDepartments { get; set; }


        #endregion
    }
}
