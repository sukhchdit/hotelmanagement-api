﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelManagement_Model.Entities
{
    public enum UserStatus { Pending = 1, Active, Suspended }
    public enum UserRole { SuperAdmin = 1, Admin, Staff, Customer }

    [Table("Users")]
    public class User : BaseEntity
    {
        public string Email { get; set; }
        public DateTime? ResetPasswordTimeStamp { get; set; }
        public string ResetPasswordKey { get; set; }
        public UserStatus UserStatus { get; set; }
        public UserRole UserRole { get; set; }
        public DateTime? LastLoginAt { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }

        //---Foreign Keys
        public IList<UserDepartment> UserDepartments { get; set; }

        [NotMapped]
        public string Password { get; set; }
    }
}
