﻿using System.Collections.Generic;

namespace HotelManagement_Model.Entities
{
    public class Department:BaseEntity
    {
        public string Name { get; set; }
        public IList<UserDepartment> UserDepartments { get; set; }
    }
}
