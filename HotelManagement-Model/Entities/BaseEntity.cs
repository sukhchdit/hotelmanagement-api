﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HotelManagement_Model.Entities
{
    public  class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
        public bool Status { get; set; }
    }
}
