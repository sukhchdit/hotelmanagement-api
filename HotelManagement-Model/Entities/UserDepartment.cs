﻿using System;

namespace HotelManagement_Model.Entities
{
    public class UserDepartment:BaseEntity
    {
        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid DepartmentId { get; set; }
        public Department Department { get; set; }
    }
}
