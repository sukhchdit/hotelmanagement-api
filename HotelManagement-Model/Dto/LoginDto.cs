﻿using HotelManagement_Model.Entities;

namespace HotelManagement_Model.Dto
{
    public class LoginDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }
        public UserRole UserRole { get; set; }
    }
}
