using HotelManagement_Model.Extension;
using HotelManagement_Repository;
using HotelManagement_Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace HotelManagement_WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
               .AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddCustomHttpContextAccessor();
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyHeader()
                       .AllowAnyMethod()
                       .SetIsOriginAllowed((host) => true)
                       .AllowCredentials();
            }));

            var config = Configuration.GetConnectionString("DefaultConnection");
            BaseService.RegisterTypes(services, config);
            BaseService.RegisterJWTToken(services, Configuration);

            //services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "HotelManagement_WebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("MyPolicy");

            app.UseAuthentication();

            app.UseAuthorization();

            // IApplicationBuilder.Use
            app.Use(
                next =>
                {
                    return async ctx =>
                    {
                        await next(ctx);
                    };
                });

            // Use extension
            app.Use(async (ctx, next) =>
            {
                await next();
            });

            // Use extension with RequestServices
            app.Use(
                async (ctx, next) =>
                {
                    var greeter = ctx.RequestServices.GetService<IRepository>();
                    await next();
                });

            //app.UseMiddleware<ApiLoggingMiddlewareService>();

            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Account}/{id?}");
            });
        }
    }
}
