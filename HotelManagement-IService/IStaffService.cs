﻿using HotelManagement_Model.Dto;
using HotelManagement_Model.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HotelManagement_IService
{
    public interface IStaffService
    {
        Task CreateSuperAdmin();
        Task DeleteStaff(Staff obj);
        Task UpdateStaffStatus(Guid userId);
        Staff GetStaffByIdWithoutStatus(Guid id);
        Staff GetStaffById(Guid id);
        Staff GetStaffByEmail(string email);
        Staff GetStaffByEmailWithoutStatus(string email);
        IEnumerable<Staff> GetAll(UserRole role);
        Staff StaffAuthenticate(LoginDto model);
        Task<Staff> UpdateLastLoginTime(Staff model);
        string GetStaffToken(Staff staff);
        Task UpdateStaff(Staff model);
        Staff GetActiveStaffById(Guid id);
        Task<Staff> CreateStaff(Staff staff);
        Task UpdatePassword(Staff obj, string password);
        Task UpdatePassword(string newpassword, Staff staff);
        Task UpdateStaffDepartment(Department model, Staff staff);
        Task Update(Staff userParam);
        Task ActivateUser(string email);
        void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt);
        Task DeleteStaff(Guid id);
    }
}
