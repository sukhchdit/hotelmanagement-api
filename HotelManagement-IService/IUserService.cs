﻿using HotelManagement_Model.Entities;
using System;

namespace HotelManagement_IService
{
    public interface IUserService
    {
        User GetById(Guid id);
    }
}
